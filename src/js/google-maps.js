 import loadGoogleMapsAPI from 'load-google-maps-api'

 loadGoogleMapsAPI({
   key: 'AIzaSyD3iE9jwqxSlpud-GewdAu8VtRokAG0sMc'
 }).then((googleMaps) => {
   var mamarigakoKulturgunea = {lat: 43.3326026, lng: -3.0387779}

   var map = new googleMaps.Map(document.getElementById('map'), {
     center: mamarigakoKulturgunea,
     scrollwheel: false,
     zoom: 15
   })

   var marker = new googleMaps.Marker({
     map: map,
     position: mamarigakoKulturgunea,
     title: 'Mamarigako Kulturgunea'
   })

   var infoWindow = new googleMaps.InfoWindow({
     content: '<span class="info-box">Mamarigako Kulturgunea</span>'
   })

   infoWindow.open(map, marker)
 })
