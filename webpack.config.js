var path = require('path')
var glob = require('glob')
var webpack = require('webpack')
var PurifyCSSPlugin = require('purifycss-webpack')
var HtmlWebpackPlugin = require('html-webpack-plugin')
var CleanWebpackPlugin = require('clean-webpack-plugin')
var ExtractTextPlugin = require('extract-text-webpack-plugin')

var inProduction = (process.env.NODE_ENV === 'production')

module.exports = {
  entry: {
    promise: 'core-js/fn/promise',
    main: './src/main.js'
  },

  output: {
    path: path.resolve(__dirname, './dist'),
    filename: '[name].[chunkhash].js'
  },

  module: {
    rules: [
      {
        test: /\.html$/,
        loader: 'html-loader'
      },
      { test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader' },
      {
        test: /\.(s[ac]ss)$/,
        use: ExtractTextPlugin.extract({
          use: [
            {
              loader: 'css-loader',
              options: {
                discardComments: {
                  removeAll: true
                }
              }
            },
            'sass-loader'
          ],
          fallback: 'style-loader'
        })
      },

      {
        test: /\.(svg|eot|ttf|woff|woff2)$/,
        loader: 'file-loader',
        options: {
          name: './fonts/[name].[hash].[ext]'
        }
      },

      {
        test: /\.(png|jpe?g|gif)$/,
        loaders: [
          {
            loader: 'file-loader',
            options: {
              name: './images/[name].[hash].[ext]'
            }
          },
          'img-loader'
        ]
      }
    ]
  },

  plugins: [
    new CleanWebpackPlugin(['dist'], {
      root: __dirname,
      verbose: true,
      dry: false
    }),

    new ExtractTextPlugin('[name].[chunkhash].css'),

    new PurifyCSSPlugin({
      paths: glob.sync(path.join(__dirname, './src/html/index.html')),
      minimize: inProduction
    }),

    new HtmlWebpackPlugin({
      template: './src/html/index.html'
    })
  ]
}
